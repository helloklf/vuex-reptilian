/**
 * Created by helloklf on 2016/8/18.
 */
package main;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import http.LocalDB;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends AbstractVerticle {

    MongoClient mongo;
    private static MongodProcess MONGO;
    private static int MONGO_PORT = 27017;

    @BeforeClass
    public static void initialize() throws IOException {
        MongodStarter starter = MongodStarter.getDefaultInstance();
        IMongodConfig mongodConfig = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(MONGO_PORT, Network.localhostIsIPv6()))
                .build();
        MongodExecutable mongodExecutable =
                starter.prepare(mongodConfig);
        MONGO = mongodExecutable.start();
    }

    @AfterClass
    public static void shutdown() {
        MONGO.stop();
    }

    //定时更新数据库
    void autoUpdateDBTask() {
        VertxOptions options = new VertxOptions();
        options.setWorkerPoolSize(150);
        options.setEventLoopPoolSize(200);
        options.setInternalBlockingPoolSize(150);
        Vertx myVertx = Vertx.vertx(options);
        DeploymentOptions options1 = new DeploymentOptions();
        options1.setWorkerPoolSize(150);
        myVertx.deployVerticle("main.AutoUpdateVertx", options1);
        myVertx.executeBlocking((h) -> {
            while (true) {
                new local.LocalDB().updateDB(
                        config(),
                        mongo,
                        hr -> {
                            System.out.println(">>> AutoUpdate FtpData ,EndTime : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            new LocalDB(mongo).updateDB();
                        }
                );
                try {
                    Thread.sleep(Integer.parseInt(config().getString("poll_timeseconds", "5400000")));//轮询周期-默认1.5小时
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, false, (result) -> {
        });
    }


    //路由配置
    void routeConfig(Router router) {
        router.route().handler(BodyHandler.create());

        //API：应用来源查询
        router.route("/api/query/source").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext context) {
                JsonArray datas = config().getJsonArray("API_SourceInfo", new JsonArray());
                String data = Json.encodePrettily(
                        new JsonObject()
                                .put("status", "1")
                                .put("result", datas.size())
                                .put("msg", "请求成功！")
                                .put("datas", datas)
                );
                context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(data);
            }
        });

        //API：获取本地应用分类信息
        router.route("/api/query/classify").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext context) {
                new local.LocalDB().findClassify(
                        mongo,
                        r -> {
                            JsonArray datas = r.result();
                            datas = datas == null ? new JsonArray() : datas;
                            String data = Json.encodePrettily(
                                    new JsonObject()
                                            .put("status", r.succeeded() ? "1" : "0")
                                            .put("result", datas.size())
                                            .put("msg", r.succeeded() ? "请求成功！" : "数据查询失败！")
                                            .put("datas", datas)
                            );

                            context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(data);
                        }
                );
            }
        });

        router.route("/api/query/deleteCache").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext context) {
                int data = Integer.parseInt(context.request().getParam("appid"));
                JsonObject query = new JsonObject().put("status", "1").put("msg", "请求成功");
                context.response().putHeader("Content-type", "text/json;charset=UTF-8")
                        .end(Json.encodePrettily(query));
            }
        });

        //API：查询更新
        router.route("/api/query/findupdate").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext context) {
                try {

                    HttpServerRequest request = context.request();

                    Handler<AsyncResult<JsonObject>> handler = new Handler<AsyncResult<JsonObject>>() {
                        @Override
                        public void handle(AsyncResult<JsonObject> r) {
                            String json = Json.encode(r.result());
                            context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(json);

                        }
                    };
                    JsonObject queryInfo = new JsonObject();

                    String appID = request.getParam("appid");
                    if (appID != null && !((appID = appID.trim()).equals("")) && !((appID = appID.trim()).equals("0")) && !((appID = appID.trim()).equals("*"))) {
                        queryInfo.put("appid", appID);
                    }

                    String classify = request.getParam("classify");
                    if (!(classify == null || ((classify = classify.trim()).equals("")) || classify.equals("0"))) {
                        //classifyInfo = new JsonObject().put("appClassify",classify);
                        //queryInfo.put("appClassify",classify);
                    } else
                        classify = null;

                    String packageNameParam = request.getParam("packageName");
                    String packageName = URLDecoder.decode(packageNameParam == null ? "" : packageNameParam, "UTF-8");
                    if (!(packageName == null || ((packageName = packageName.trim()).equals("")) || packageName.equals("*"))) {
                        queryInfo.put("packageName", packageName);
                    }

                    String appNameParam = request.getParam("appName");
                    String appName = URLDecoder.decode(appNameParam == null ? "" : appNameParam, "UTF-8");
                    if (!(appName == null || ((appName = appName.trim()).equals("")) || appName.equals("*"))) {
                        queryInfo.put("appName", new JsonObject().put("$regex", "(?i)" + appName));
                    }

                    String uv = request.getParam("unknownVersion");
                    boolean unknown = uv != null && uv != "";//(uv==null?"":uv.trim()).equals("1");
                    String lv = request.getParam("localVersion");
                    boolean localVersion = lv != null && lv != ""; //;(lv==null?"":lv.trim()).equals("1");

                    String st = request.getParam("source");
                    FindUpdateOptions findUpdateOptions = new FindUpdateOptions();
                    findUpdateOptions.mongoClient = mongo;
                    findUpdateOptions.queryInfo = queryInfo;
                    findUpdateOptions.findOptions = new FindOptions();
                    findUpdateOptions.appName = appName;
                    findUpdateOptions.appID = appID;
                    findUpdateOptions.unknownVersion = unknown;
                    findUpdateOptions.localVersion = localVersion;
                    findUpdateOptions.classify = classify;
                    findUpdateOptions.handler = handler;
                    findUpdateOptions.packageName = packageName;
                    findUpdateOptions.resultFields = config().getJsonObject("API_ResultFields");
                    String dateLT = request.getParam("updateDateLT");
                    String dateGT = request.getParam("updateDateGT");

                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-M-d");
                    SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");

                    if (!(dateLT == null || (dateLT = dateLT.trim()).equals(""))) {
                        findUpdateOptions.updateDateLT = dateFormat2.format(dateFormat1.parse(dateLT));
                    }
                    if (!(dateGT == null || (dateGT = dateGT.trim()).equals(""))) {
                        findUpdateOptions.updateDateGT = dateFormat2.format(dateFormat1.parse(dateGT));
                    }

                    LocalDB.SourceType sourceTypeCopy = null;
                    if (!(st == null || (st = st.trim()).equals("") || st.equals("0"))) {
                        LocalDB.SourceType sourceType = LocalDB.SourceType.valueOf(st);
                        sourceTypeCopy = sourceType;
                        findUpdateOptions.setSourceType(sourceType);
                    }

                    new local.LocalDB().findUpdate(findUpdateOptions);

                } catch (UnsupportedEncodingException e) {
                    new local.LocalDB().findUpdateError(r -> {
                        context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(Json.encode(r.result()));
                    });
                } catch (ParseException e) {
                    new local.LocalDB().findUpdateError(r -> {
                        context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(Json.encode(r.result()));
                    });
                }
            }
        });

        //提交更新日志
        router.route("/api/query/uploadlog").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext context) {
                new http.LocalDB(mongo).setUpdateItemStatus(
                        context.request().getParam("id"),
                        r -> {
                            context.response().putHeader("Content-type", "text/json;charset=UTF-8").end(Json.encodePrettily(
                                    new JsonObject()
                                            .put("status", r.succeeded() ? "1" : "0")
                                            .put("msg", r.succeeded() ? "请求成功！" : "数据查询失败！")
                                    )
                            );
                        });
            }
        });

        router.route("/api/query/remove").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext routingContext) {
                try {


                    String data = routingContext.request().getParam("appid");
                    List<Boolean> appList = new ArrayList<Boolean>();
                    Handler<AsyncResult<Boolean>> resultHandler;
                    String[] appIdSplit = data.split(",");
                    for (int i = 0; i < appIdSplit.length; i++) {
                        String appId = appIdSplit[i];
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                        String time = df.format(new Date());
                        mongo.find("phone580", new JsonObject().put("appid", appId), o -> {
                            JsonObject result2 = o.result().get(0);
                            String test1 = result2.getString("appName");
                            String test2 = result2.getString("versionCode");
                            mongo.insert("limtpackage", new JsonObject()
                                    .put("appid", appId)
                                    .put("deletetime", time)
                                    .put("appName", result2.getString("appName"))
                                    .put("appClassify", result2.getString("appClassify"))
                                    .put("platform", result2.getString("platform"))
                                    .put("versionName", result2.getString("versionName"))
                                    .put("versionCode", result2.getString("versionCode"))
                                    .put("packageName", result2.getString("packageName")), r -> {
                                if (r.succeeded()) {
                                    mongo.remove("phone580", new JsonObject().put("appid", appId), s -> {
                                        if (s.succeeded()) {
                                            appList.add(s.succeeded());
                                        }
                                    });
                                }
                            });
                        });
                    }

                    int numberFalse = 0;
                    for (Boolean tmp : appList) {
                        if (tmp) {
                            numberFalse++;
                        }
                    }
                    boolean query;
                    if (numberFalse == appList.toArray().length) {
                        query = true;
                    } else {
                        query = false;
                    }
                    routingContext.response().putHeader("Content-type", "text/json;charset=UTF-8").end(Json.encodePrettily(
                            new JsonObject()
                                    .put("status", query ? "1" : "0")
                                    .put("msg", query ? "请求成功！" : "数据查询失败！")
                    ));

                } catch (Exception e) {
                    System.out.print("请求api remove错误|" + e);
                }
            }

        });

        //静态页面访问
        router.route("/*").

                handler(StaticHandler.create(config().

                        getString("webroot", "./classes/assets")));
    }


    @Override
    public void start(Future<Void> fut) {
        mongo = MongoClient.createShared(vertx, config());

        //关闭MongoDB日志
        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        mongoLogger.setLevel(Level.SEVERE);

        //创建应用库自动更新任务
        //autoUpdateDBTask();

        Router router = Router.router(vertx);
        routeConfig(router);

        vertx.createHttpServer().requestHandler(router::accept).listen(
                config().getInteger("http.port", 8080),
                result -> {
                    if (result.succeeded()) {
                        fut.complete();
                    } else {
                        fut.fail(result.cause());
                    }
                }
        );

        /*
        VertxOptions options = new VertxOptions();
        options.setWorkerPoolSize(50);
        Vertx myVertx = Vertx.vertx(options);
        DeploymentOptions options1 = new DeploymentOptions();
        myVertx.deployVerticle("MyTestVertx",options1);
        myVertx.executeBlocking((future)->{

        },(result)->{

        });
        */

    }
}
