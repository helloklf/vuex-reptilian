package main;

import io.vertx.core.json.JsonArray;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by helloklf on 2016/9/8.
 */
public class Tools {
    //将ArrayList<Object>转换为JsonArray
    public static <T> JsonArray ListToJsonArray(List<T> arrayList) {
        JsonArray array = new JsonArray();
        if (arrayList != null) {
            for (T item : arrayList) {
                array.add(item);
            }
        }
        return array;
    }

    //比较版本名称
    //result:
    //1:varsionName1>versionName2
    //-1:varsionName1<varsionName2
    //-2:无法比较
    //0:varsionName1==varsionName2
    //匹配规则：
    //1.0.0.0 ： 1.0.0.0 ==> 0
    //1.0.0.1 ： 1.0.0.0 ==> 1
    //1.0.0.1 ： 1.0.0   ==> 1
    //1.0.0 ： 1.0.0.1   ==>-1
    public static int versionNameCompare(String versionName1, String versionName2) {
        try{

            boolean isVString2 = versionName2.toLowerCase().contains("v".toLowerCase());
            if (isVString2) {
                versionName2 = filterString(versionName2);
                boolean isVString1 = versionName1.toLowerCase().contains("v".toLowerCase());
                if (isVString1) {
                    versionName1 = filterString(versionName1);
                }
            }
            if (versionName1 == null || versionName2 == null || (versionName1 = versionName1.trim()).equals("") || (versionName2 = versionName2.trim()).equals("")) {
                return -2;
            } else if (versionName1.equals(versionName2))
                return 0;


            //加上versionCode比对
            //string[] test = versionName1.equals(versionName2);
            String[] items1 = versionNameSplit(versionName1);
            String[] items2 = versionNameSplit(versionName2);

            int len1 = items1.length;
            int len2 = items2.length;
            int minLen = len1 < len2 ? len1 : len2;

            for (int i = 0; i < minLen; i++) {
                try {
                    int val1Lenght = items1[i].length();
                    int val2Lenght = items2[i].length();
                    int minLen1 = val1Lenght < val2Lenght ? val2Lenght - val1Lenght : val1Lenght - val2Lenght;
                    if (minLen1 < 2) {
                        int val1 = Integer.parseInt(items1[i]);
                        int val2 = Integer.parseInt(items2[i]);
                        if (val1 > val2)
                            return 1;
                        else if (val2 > val1)
                            return -1;
                    }
                } catch (Exception ex) {
                    //
                }
            }

            if (len1 > len2)
                return 1;
            else if (len2 > len1)
                return -1;
            else {return 0;}
        }
        catch (Exception ex){
            return  -2;
        }
    }

    public static int IsVersionCode(String versionCode1, String versionCode2) {
        int versionCode1Len = Integer.parseInt(versionCode1);
        int versionCode2Len = Integer.parseInt(versionCode2);
        if (versionCode1Len > versionCode2Len) {
            return 1;
        } else if (versionCode1Len < versionCode2Len) {
            return -1;
        } else {
            return 0;
        }
    }

    public static String filterString(String versionName) {

        boolean isV = versionName.substring(0, 1).toLowerCase().contains("v".toLowerCase());
        String newVersionName = null;
        if (isV) {
            newVersionName = versionName.substring(1);
        }
        return isV ? newVersionName : versionName;
    }

    static String[] versionNameSplit(String versionName) {
        if (versionName == null)
            return new String[]{};
        String[] items = versionName
                .toLowerCase()
                .replace("\\:", ".")
                .replace("\\-", ".")
                .replace("\\_", ".")
                .replaceAll("[^\\d.]", "")
                .split("\\.");
        return items;
    }

    //发布日期处理
    public static String dateTimeToDateTime(String dateTime) {
        //[\d]{4}年[\d]{1,2}月[\d]{1,2}日//yyyy年MM月dd日，yyyy年M月d日
        //[\d]{4}[\-][\d]{1,2}[\-][\d]{1,2}//yyyy-MM-dd，yyyy-M-d
        //[\d]{4}[\.][\d]{1,2}[\.][\d]{1,2}//yyyy.MM.dd，yyyy.M.d
        //[\d]{10}//时间戳，秒
        //[\d]{13}//时间戳，毫秒
        if (dateTime != null) {
            String date = dateTime.trim();
            if (date.matches("[\\d]{4}[\\-][\\d]{1,2}[\\-][\\d]{1,2}")) {
                Pattern p = Pattern.compile("[\\d]{4}[\\-][\\d]{1,2}[\\-][\\d]{1,2}");
                Matcher m = p.matcher(dateTime);
                m.find();
                date = m.group();
            } else if (date.matches("[\\d]{4}年[\\d]{1,2}月[\\d]{1,2}日")) {
                Pattern p = Pattern.compile("[\\d]{4}年[\\d]{1,2}月[\\d]{1,2}日");
                Matcher m = p.matcher(dateTime);
                m.find();
                date = m.group().replace('年', '-').replace('月', '-').replace("日", "");
            } else if (date.matches("[\\d]{4}[\\.][\\d]{1,2}[\\.][\\d]{1,2}")) {
                Pattern p = Pattern.compile("[\\d]{4}[\\.][\\d]{1,2}[\\.][\\d]{1,2}");
                Matcher m = p.matcher(dateTime);
                m.find();
                date = m.group().replace('.', '-');
            } else if (date.matches("[\\d]{10}")) {
                Pattern p = Pattern.compile("[\\d]{10}");
                Matcher m = p.matcher(dateTime);
                m.find();
                date = new SimpleDateFormat("yyyy-MM-dd").format(new Date(Long.parseLong(m.group()) * 1000));
            } else if (date.matches("[\\d]{13}")) {
                Pattern p = Pattern.compile("[\\d]{13}");
                Matcher m = p.matcher(dateTime);
                m.find();
                date = new SimpleDateFormat("yyyy-MM-dd").format(new Date(Long.parseLong(m.group())));
            }
            return date;
        }
        return dateTime;
    }


}
