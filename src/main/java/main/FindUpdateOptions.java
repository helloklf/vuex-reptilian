package main;

import http.LocalDB;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

/**
 * Created by helloklf on 2016/9/17.
 */
public class FindUpdateOptions {
    //
    public MongoClient mongoClient;

    //
    public JsonObject queryInfo;

    //是否显示未知版本
    public Boolean unknownVersion = false;

    //是否显示本地版本
    public Boolean localVersion = false;

    //应用程序名称
    public String appName;

    //应用ID
    public String appID;

    public String packageName;

    //分类
    public String classify;

    //
    public FindOptions findOptions;

    //yyyyMMdd
    public String updateDateLT;

    //yyyyMMdd
    public String updateDateGT;

    boolean sourceTypeSeted = false;
    LocalDB.SourceType sourceType;

    //设置来源搜索条件
    public void  setSourceType(LocalDB.SourceType sourceType){
        this.sourceType = sourceType;
        sourceTypeSeted = true;
    }

    //结果字段集合
    public JsonObject resultFields;


    //获取来源搜索条件
    public int getSourceType(){
        if(!sourceTypeSeted)
            return -1;
        return (sourceType.ordinal());
    }

    //搜索完成后回调
    public Handler<AsyncResult<JsonObject>> handler;


    public JsonObject GetQueryInfo(JsonObject localApp){
        FindUpdateOptions options = this;

        JsonObject queryInfo = new JsonObject();
        String pkgName = localApp.getString("packageName",null);
        if(pkgName!=null&&!pkgName.trim().equals(""))
            queryInfo.put("packageName",pkgName);//queryInfo.put("data.pkgName",pkgName);
        else
            queryInfo.put("data.appName",localApp.getString("appName"));

        if((options.updateDateLT!=null&&!options.updateDateLT.equals(""))
                &&
                (options.updateDateGT!=null&&!options.updateDateGT.trim().equals(""))){
            //{'$and':[{'example.a':{'$gt':1}},{'example.b':{'$gt':2}}]},
            queryInfo.put("$and",
                    new JsonArray()
                            .add(new JsonObject().put("datetime",new JsonObject().put("$lte",options.updateDateLT)))
                            .add(new JsonObject().put("datetime",new JsonObject().put("$gte",options.updateDateGT)))
            );
        }
        else if(options.updateDateLT!=null&&!options.updateDateLT.equals("")){
            queryInfo.put("datetime",new JsonObject().put("$lte",options.updateDateLT));
        }
        else if(options.updateDateGT!=null&&!options.updateDateGT.trim().equals("")){
            queryInfo.put("datetime",new JsonObject().put("$gte",options.updateDateGT));
        }


        if(!localVersion)
            queryInfo.put("localUpdated",new JsonObject().put("$ne","1"));

        return  queryInfo;
    }
}
