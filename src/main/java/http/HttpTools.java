package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 * Created by helloklf on 2016/8/22.
 */
public class HttpTools {
    public String GetContent(String url,int reDo,int maxReDo){
        URL realUrl = null;
        StringBuilder builder;
        try {
            builder = new StringBuilder();
            realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            connection.setConnectTimeout(2*60*1000);//2分钟超时
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();

            InputStream inputStream = connection.getInputStream();

            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                    inputStream, Charset.forName("UTF-8")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
            bufferedReader.close();
            inputStream.close();
            return builder.toString();

        } catch (MalformedURLException e) {
            if(reDo<maxReDo){
                return GetContent(url,(reDo+1),maxReDo);
            }
            else
                System.out.println("MalformedURLException:"+url);
            //e.printStackTrace();
        } catch (IOException e) {
            try {
                Thread.sleep(3000);
            }
            catch(Exception ex) {   }

            if(reDo<maxReDo){
                return GetContent(url,(reDo+1),maxReDo);
            }
            else
                System.out.println("IOException:"+url);
            //e.printStackTrace();
        }
        return "";
    }
}
