package http;

import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;
import io.vertx.ext.mongo.WriteOption;
import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by helloklf on 2016/8/25.
 */
public interface StoreInterfaceAsync {

    void getAppTypes(Handler<AsyncResult<JsonArray>> typesResult);

    void getGameTypes(Handler<AsyncResult<JsonArray>> typesResult);

    void getAppByType(Handler<AsyncResult<JsonArray>> appsResult, JsonObject type);

    void getGameByType(Handler<AsyncResult<JsonArray>> gamesResult, JsonObject type);

    UpdateOptions options = new UpdateOptions(true, false);

    default void insertDB(MongoClient mongo, Handler<AsyncResult<Boolean>> inserted, LocalDB.SourceType sourceType, boolean isGame, String typeId, JsonArray data) {
        String date = dateFormat.format(new Date());
        String typeName = getCollectionNameByType(sourceType);
        List<Future> futures = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            JsonObject appItem = data.getJsonObject(i);
            if (appItem.size() < 1)
                continue;
            JsonObject document =
                    new JsonObject()
                            //.put("isgame", isGame ? "1" : "0")
                            .put("datetime", date)
                            .put("typeid", typeId)
                            //.put("localUpdated", "0")
                            .put("source", typeName)
                            .put("versionName", appItem.getString("versionName"))
                            .put("packageName", appItem.getString("pkgName"))
                            .put("data", appItem.put("publishDate", main.Tools.dateTimeToDateTime(
                                    appItem.getString("apkPublishTimeText") == null ? appItem.getLong("apkPublishTime").toString() : appItem.getString("apkPublishTimeText")
                            )));
            Future future = Future.future();
            futures.add(future);

            //方案二：更新（如果版本已经存在）或新增记录
            mongo.updateWithOptions(
                    typeName,
                    new JsonObject()
                            .put("_id", typeName + "_" + appItem.getString("pkgName"))
                            .put("versionName", new JsonObject().put("$ne", appItem.getString("versionName"))),
                    new JsonObject().put("$set", document.put("_id", typeName + "_" + appItem.getString("pkgName")).put("localUpdated", "0")),
                    options,
                    r -> future.complete()
            );
        }

        CompositeFuture.all(futures).setHandler(r -> {
            inserted.handle(Future.<Boolean>succeededFuture(true));
        });
    }

    default void storeData1(MongoClient mongo, LocalDB.SourceType sourceType, JsonObject appItem) {
        try {

        String date = dateFormat.format(new Date());
        String typeName = getCollectionNameByType(sourceType);
        JsonObject dataObject = new JsonObject();
        dataObject
                .put("appName", appItem.getString("appName"))
                .put("fileSizeMB", appItem.getString("fileSizeMB"))
                .put("versionCode", appItem.getString("versionCode"))
                .put("appId", appItem.getString("appId"))
                .put("appName", appItem.getString("appName"))
                .put("iconUrl", appItem.getString("iconUrl"))
                .put("apkUrl", appItem.getString("apkUrl"))
                .put("categoryName", appItem.getString("categoryName"))
                .put("pkgName", appItem.getString("pkgName"))
                .put("publishDate", appItem.getString("publishDate"));
        JsonObject document = new JsonObject()
                .put("datetime", date)
                //.put("localUpdated", "0")
                .put("source", typeName)
                .put("versionName", appItem.getString("versionName"))
                .put("packageName", appItem.getString("pkgName"))
                .put("data", dataObject
                );

        mongo.updateWithOptions(
                typeName,
                new JsonObject()
                        .put("_id", typeName + "_" + appItem.getString("pkgName"))
                        .put("versionName", new JsonObject().put("$ne", appItem.getString("versionName"))),
                new JsonObject().put("$set", document.put("_id", typeName + "_" + appItem.getString("pkgName")).put("localUpdated", "0")),
                options, r -> {

                });
        }
        catch (Exception e)
        {
            System.out.print("yingyongbao更新错误|"+e);
        }
    }

    default void storeData(MongoClient mongo, LocalDB.SourceType sourceType, Handler<AsyncResult<Boolean>> resultHandler, JsonObject appItem) {
        String date = dateFormat.format(new Date());
        String typeName = getCollectionNameByType(sourceType);
        JsonObject data = new JsonObject();

        JsonObject document = new JsonObject()
                .put("datetime", date)
                //.put("localUpdated", "0")
                .put("source", typeName)
                .put("versionName", appItem.getString("versionName"))
                .put("packageName", appItem.getString("pkgName"))
                .put("data", appItem.put("publishDate", main.Tools.dateTimeToDateTime(
                        appItem.getString("apkPublishTimeText") == null ? appItem.getLong("apkPublishTime").toString() : appItem.getString("apkPublishTimeText")
                )));

        mongo.updateWithOptions(
                typeName,
                new JsonObject()
                        .put("_id", typeName + "_" + appItem.getString("pkgName"))
                        .put("versionName", new JsonObject().put("$ne", appItem.getString("versionName"))),
                new JsonObject().put("$set", document.put("_id", typeName + "_" + appItem.getString("pkgName")).put("localUpdated", "0")),
                options, r ->
                        resultHandler.handle(Future.succeededFuture()));
    }

    default void storeData(MongoClient mongo, LocalDB.SourceType sourceType, Handler<AsyncResult<Boolean>> resultHandler, boolean isGame, String typeId, JsonArray data) {
        insertDB(mongo, inset -> {
            resultHandler.handle(Future.<Boolean>succeededFuture(true));
        }, sourceType, isGame, typeId, data);
    }

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    default String getCollectionNameByType(LocalDB.SourceType sourceType) {
        String collectionName = null;
        switch (sourceType) {
            case yingyongbao: {
                collectionName = "yingyongbao";
                break;
            }
            case wandoujia: {
                collectionName = "wandoujia";
                break;
            }
            case pp: {
                collectionName = "pp";
                break;
            }
        }
        return collectionName;
    }

    //更新本地数据库
    default void updateLocalDB(MongoClient mongo, LocalDB.SourceType sourceType, Handler<AsyncResult<Boolean>> resultHandler) {
        String date = dateFormat.format(new Date());
        String collectionName = getCollectionNameByType(sourceType);
        Handler<AsyncResult<CompositeFuture>> h = (r) -> {
            System.out.println(">>>  " + collectionName + " [Game] Update , Completeness : 100%");
            getAppTypes(types -> {
                JsonArray typesJson = types.result();
                final int[] complateCount = {0};
                List<Future> futures = new ArrayList<Future>();
                for (int i = 0; i < typesJson.size(); i++) {
                    JsonObject type = typesJson.getJsonObject(i);
                    Future future = Future.future();
                    futures.add(future);
                    getAppByType((apps) -> {
                        complateCount[0]++;
                        System.out.println(">>>  " + collectionName + " [App] Update , Completeness : " + complateCount[0] + "/" + typesJson.size() + " ...");
                        future.complete();
                    }, type);
                }

                CompositeFuture.all(futures).setHandler(xxx -> {
                    System.out.println(">>>  " + collectionName + " [App] Update , Completeness : 100%");
                    resultHandler.handle(Future.<Boolean>succeededFuture(true));
                });

            });
        };

        getGameTypes(types -> {
            JsonArray typesJson = types.result();
            final int[] complateCount = {0};
            List<Future> futures = new ArrayList<Future>();

            for (int i = 0; i < typesJson.size(); i++) {
                JsonObject type = typesJson.getJsonObject(i);
                Future future = Future.future();
                futures.add(future);
                getAppByType((apps) -> {
                    complateCount[0]++;
                    System.out.println(">>>  " + collectionName + " [Game] Update , Completeness : " + complateCount[0] + "/" + typesJson.size() + " ...");
                    future.complete();
                }, type);
            }

            CompositeFuture.all(futures).setHandler(h);
        });
    }
}
