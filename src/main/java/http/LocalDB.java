package http;

import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by helloklf on 2016/8/24.
 */
public class LocalDB {

    MongoClient mongo;

    public LocalDB(MongoClient db) {
        this.mongo = db;
    }

    //查找一个
    public void findOne(JsonObject query, JsonObject resultFielfs, Handler<AsyncResult<JsonArray>> handler) {
        find(query, resultFielfs, 1, handler);
    }

    //查找一个
    public void findOne(JsonObject query, SourceType sourceType, JsonObject resultFielfs, Handler<AsyncResult<JsonArray>> handler) {
        find(query, sourceType, resultFielfs, 1, handler);
    }

    //查找
    public void find(JsonObject query, SourceType sourceType, JsonObject fields, int limit, Handler<AsyncResult<JsonArray>> handler) {
        FindOptions findOptions = new FindOptions();
        findOptions.setSort(new JsonObject().put("datetime", -1));//根据数据抓取时间倒序
        findOptions.setLimit(limit < 1 ? 1 : limit);
        if (fields != null && fields.size() > 0)
            findOptions.setFields(fields);

        mongo.findWithOptions(
                getCollectionNameByType(sourceType),
                query,
                findOptions,
                r -> {
                    if (!r.succeeded())
                        System.out.println(r.cause().getMessage());
                    handler.handle(Future.<JsonArray>succeededFuture(main.Tools.ListToJsonArray(r.result())));
                });
    }


    //查找
    public void find(JsonObject query, JsonObject fields, int limit, Handler<AsyncResult<JsonArray>> handler) {
        FindOptions findOptions = new FindOptions();
        findOptions.setSort(new JsonObject().put("datetime", -1));//根据数据抓取时间倒序
        findOptions.setLimit(limit < 1 ? 1 : limit);//限制结果条数，1-∞
        if (fields != null && fields.size() > 0)
            findOptions.setFields(fields);

        JsonArray allData = new JsonArray();

        mongo.findWithOptions(
                getCollectionNameByType(SourceType.yingyongbao),
                query,
                findOptions,
                r -> {
                    allData.addAll(main.Tools.ListToJsonArray(r.result()));

                    mongo.findWithOptions(
                            getCollectionNameByType(SourceType.wandoujia),
                            query,
                            findOptions,
                            wdjr -> {
                                allData.addAll(main.Tools.ListToJsonArray(wdjr.result()));

                                handler.handle(Future.<JsonArray>succeededFuture(allData));
                            });
                });
    }

    //设置更新是否已下载
    public void setUpdateItemStatus(String id, Handler<AsyncResult<Boolean>> resultHandler) {
        UpdateOptions options = new UpdateOptions();
        options.setMulti(true);
        options.setUpsert(false);

        JsonArray ids = new JsonArray();
        if (id != null) {
            String[] idList = id.trim().split("\\,");
            for (String idItem : idList) {
                ids.add(idItem);
            }
        }

        ArrayList<Future> futures = new ArrayList<>();
        for (SourceType sourceType : SourceType.values()) {
            Future future = Future.future();
            futures.add(future);
            mongo.updateWithOptions(
                    getCollectionNameByType(sourceType),
                    new JsonObject().put("_id", new JsonObject().put("$in", ids)),
                    new JsonObject().put("$set", new JsonObject().put("localUpdated", "1")),
                    options,
                    r -> {
                        future.complete();
                    }
            );
        }
        CompositeFuture.all(futures).setHandler(r -> {
            resultHandler.handle(Future.<Boolean>succeededFuture(true));
        });
    }

    String getCollectionNameByType(SourceType sourceType) {
        String collectionName = null;
        switch (sourceType) {
            case yingyongbao: {
                collectionName = "yingyongbao";
                break;
            }
            case wandoujia: {
                collectionName = "wandoujia";
                break;
            }
            case pp: {
                collectionName = "pp";
                break;
            }
        }
        return collectionName;
    }

    public void removeData(MongoClient mongo) {
        try {
            mongo.find("limtpackage", new JsonObject().put("appid", new JsonObject().put("$ne", "")), r -> {
                if (r.succeeded()) {
                    List<JsonObject> data = r.result();
                    Object[] dataArray = data.toArray();
                    for (int i = 0; i < dataArray.length; i++) {
                        String limtAppId = data.get(i).getString("appid");
                        mongo.remove("phone580", new JsonObject().put("appid", limtAppId), o -> {
                            if (o.failed()) {
                                System.out.print("appid|" + limtAppId);
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            System.out.print("removeData|" + e);
        }
    }

    //更新本地数据库
    public void updateDB() {
        Handler<AsyncResult<List<String>>> header = (resultInfo) ->
        {
            if (resultInfo.succeeded()) {
                List<String> collections = resultInfo.result();
                if (collections.contains("limtpackage")) {
                    removeData(mongo);
                }
            }
        };
        mongo.getCollections(header);
        System.out.println(">>> Application library update started...");
        System.out.println(">>> This operation takes some time, please wait for a moment...\n\n\n");

        Future futureYingyongbao = Future.future();
        Vertx.vertx().executeBlocking(
                h -> {
                    //SourceType source = SourceType.yingyongbao;
                    new http.yingyongbao.Tools(mongo).updateLocalDB(resultHeadle->
                            h.complete());


                },
                false,
                result -> {
                    System.out.println("\n\n>>> { YingYongBao [App&Game] Update , All Complated! }\n");
                    futureYingyongbao.complete();
                }
        );
        futureYingyongbao.setHandler(r -> {
            Vertx.vertx().executeBlocking(
                    h -> {
                        SourceType source = SourceType.wandoujia;
                        new http.wandoujia.Tools(mongo).updateLocalDB(
                                mongo,
                                source,
                                updateResult -> h.complete()
                        );
                    },
                    false,
                    result -> {
                        System.out.println("\n\n>>>{ Wandoujia [App&Game] Update , All Complated! }\n");

                        System.out.println("\n\n>>>{ Http-LocalDB [App&Game] Update , All Complated! }\n");
                        System.out.println(">>> AutoUpdate HttpData ,EndTime : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    }
            );
        });
    }

    public enum SourceType {
        yingyongbao,
        wandoujia,
        pp
    }
}
