package http.yingyongbao;

import http.HttpTools;
import http.LocalDB;
import http.StoreInterfaceAsync;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lidong on 2016/12/26.
 */
public class Tools implements StoreInterfaceAsync {

    MongoClient mongo;

    public Tools(MongoClient mongoClient) {
        this.mongo = mongoClient;
    }

    HttpTools httpTools = new HttpTools();

    //应用宝详情页面地址
    String appDetailsUrl = "http://sj.qq.com/myapp/detail.htm?apkName=";

    //获取App分类信息页面地址
    String appTypesUrl = "http://www.wandoujia.com/category/app";
    //获取游戏分类信息页面地址
    String gameTypeUrl = "http://www.wandoujia.com/category/game";
    //获取应用地址（含参数占位符）
    String appsUrl = "http://www.wandoujia.com/category/%s_%s";
    //获取游戏地址（含参数占位符）
    String gamesUrl = "http://www.wandoujia.com/category/%s_%s";

    //获取分类信息（getGmae：为true时获取游戏，否则获取应用）
    private void getTypes(Handler<AsyncResult<JsonArray>> typesResult, boolean getGame) {
        Vertx.currentContext().executeBlocking(h -> {
            Document document = Jsoup.parse(httpTools.GetContent(getGame ? gameTypeUrl : appTypesUrl, 0, 3));
            Elements elements = document.select(".container  .parent-cate>a");
            JsonArray jsonArray = new JsonArray();
            String url = null;
            String typeid;
            for (Element a : elements) {
                try {
                    url = a.attr("href");
                    if (url.equals("javascript:void(0);"))
                        continue;
                    typeid = url.substring(url.lastIndexOf("category/") + "category/".length());
                    jsonArray.add(new JsonObject().put("name", a.text()).put("url", url).put("typeid", typeid));
                } catch (Exception e) {
                    System.out.println("ErrorName:WanDouJia Find Type - " + url + " | " + e.getMessage());
                }
            }
            h.complete(jsonArray);
        }, false, typesResult);
    }

    //获取分类信息
    public void getAppTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(typesResult, false);
    }

    //获取游戏分类
    public void getGameTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(typesResult, true);
    }

    @Override
    public void getAppByType(Handler<AsyncResult<JsonArray>> appsResult, JsonObject type) {

    }

    @Override
    public void getGameByType(Handler<AsyncResult<JsonArray>> gamesResult, JsonObject type) {

    }

    class GetAllByTypeThread {
        JsonArray allApps;
        JsonObject type;

        public GetAllByTypeThread(JsonObject type) {
            this.type = type;
        }


        class GetPageAppsThread {
            String url;

            public GetPageAppsThread(String url) {
                this.url = url;
            }

            public JsonArray GetPageApps() {
                JsonArray apps = new JsonArray();
                Document document = Jsoup.parse(httpTools.GetContent(url, 0, 3));
                Elements elements = document.select("#j-tag-list>li");

                for (Element element : elements) {
                    apps.add(getOneDetails(element.attr("data-pn")));
                }
                return apps;
            }
        }
    }

    static boolean exists(String URLName) {

        try {

            //设置此类是否应该自动执行 HTTP 重定向（响应代码为 3xx 的请求）。

            HttpURLConnection.setFollowRedirects(false);

            //到 URL 所引用的远程对象的连接

            HttpURLConnection con = (HttpURLConnection) new URL(URLName)

                    .openConnection();

           /* 设置 URL 请求的方法， GET POST HEAD OPTIONS PUT DELETE TRACE 以上方法之一是合法的，具体取决于协议的限制。*/

            con.setRequestMethod("HEAD");

            //从 HTTP 响应消息获取状态码

            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);

        } catch (Exception e) {

            e.printStackTrace();

            return false;

        }

    }

    public void updateLocalDB( Handler<AsyncResult<Boolean>> resultHandler) {
        new local.LocalDB().findAllPackageName(mongo, o -> {
            List<String> result = o.result();
            if (result != null) {
                boolean yingyongbaoURL = exists("http://sj.qq.com/");
                List<Future> futures = new ArrayList<Future>();
                for (int i = 0; i < result.size(); i++) {
                    Future future = Future.future();
                    String pkg = result.get(i);
                    futures.add(future);
                    Vertx.currentContext().executeBlocking(h -> {
                        if (yingyongbaoURL) {
                            JsonObject baoApp = getOneDetails(pkg);
                            if (baoApp != null) {
                                storeData1(mongo, LocalDB.SourceType.yingyongbao, baoApp);
                            }
                        }
                        h.complete();
                    }, false, insertResult -> {

                        future.complete();
                    });
                }
                CompositeFuture.all(futures).setHandler(allComplated -> {
                    resultHandler.handle(Future.succeededFuture());
                });
            }
        });
    }

    public JsonObject getOneDetails(String packageName) {
        JsonObject appInfo = new JsonObject();
        try {
            //String httpMessage = GetHttpMessage(packageName);
            Document document = Jsoup.parse(httpTools.GetContent(appDetailsUrl + packageName, 0, 3));
            Element body = document.select(".com-container").first();
            Elements js = document.getElementsByTag("script").eq(3);//捕获js
            Element jsMessage = js.get(0);//选择需要捕获的js
            String[] jsParameter = jsMessage.toString().trim().split("\\t");
            String[] appName = jsParameter[5].split(":");
            int appNameSize = appName[1].trim().length();
            if (appNameSize >= 3) {
                String jsAppName = appName[1].trim().substring(1, appName[1].trim().length() - 2);
                int result = jsAppName.length();
                if (result == 0) {
                    appInfo = null;
                } else {
                    String version = body.select(".det-othinfo-data").first().text();//app版本号
                    String appTypeParament = body.select(".det-type-box").first().text();
                    String[] appTypeSplit = appTypeParament.split(" ");
                    String appType = appTypeSplit[1];
                    String size = body.select(".det-size").first().text();
                    String updateTime = main.Tools.dateTimeToDateTime(body.select("#J_ApkPublishTime").attr("data-apkPublishTime"));//更新时间
                    String[] appId = jsParameter[4].split(":");
                    String[] apkCode = jsParameter[3].split(":");
                    String[] appIcon = jsParameter[6].split(":");
                    String[] apkUrl = jsParameter[9].split(":");
                    appInfo.put("publishDate", updateTime);
                    appInfo.put("fileSizeMB", size);//apk文件大小
                    appInfo.put("versionName", version.substring(1));//版本号
                    appInfo.put("appId", appId[1].substring(2, appId[1].length() - 2));//应用在应用宝的ID
                    appInfo.put("versionCode", apkCode[1].substring(2, apkCode[1].length() - 2));//小版本号
                    appInfo.put("appName", appName[1].substring(2, appName[1].length() - 2));//应用名
                    appInfo.put("iconUrl", "http:/" + appIcon[2].substring(1, appIcon[2].length() - 2));//图标地址
                    appInfo.put("apkUrl", "http:/" + apkUrl[2].substring(1, apkUrl[2].length() - 2));//下载地址
                    appInfo.put("pkgName", packageName);//包名
                    appInfo.put("categoryName", appType);//类型
                    JsonArray appImgs = new JsonArray();
                    Elements imgs = body.select(".pic-img-box img");//#J_PicTurnImgBox > div:nth-child(1)
                    for (Element img : imgs) {
                        appImgs.add(img.attr("data-src"));
                    }
                    Elements newFeature = body.select(".det-app-data-info");
                    JsonArray appMessage = new JsonArray();
                    for (Element newF : newFeature) {
                        appMessage.add(newF.text());
                    }
                    appInfo.put("newFeature", appMessage);//更新内容//
                    appInfo.put("images", appImgs);
                }

            } else {
                appInfo = null;
            }
        } catch (Exception e) {
            System.out.println("ErrorName:yingyongbao|" + packageName + "|" + e);
        } finally {
            return appInfo;
        }
    }
}
