package http.wandoujia;

import http.HttpTools;
import http.LocalDB;
import http.StoreInterfaceAsync;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class Tools implements StoreInterfaceAsync {
    MongoClient mongo;

    public Tools(MongoClient mongoClient) {
        this.mongo = mongoClient;
    }

    http.yingyongbao.Tools tool2 = new http.yingyongbao.Tools(mongo);
    HttpTools httpTools = new HttpTools();

    //获取App分类信息页面地址
    String appTypesUrl = "http://www.wandoujia.com/category/app";
    //获取游戏分类信息页面地址
    String gameTypeUrl = "http://www.wandoujia.com/category/game";
    //获取应用地址（含参数占位符）
    String appsUrl = "http://www.wandoujia.com/category/%s_%s";
    //获取游戏地址（含参数占位符）
    String gamesUrl = "http://www.wandoujia.com/category/%s_%s";

    //获取分类信息（getGmae：为true时获取游戏，否则获取应用）
    private void getTypes(Handler<AsyncResult<JsonArray>> typesResult, boolean getGame) {
        Vertx.currentContext().executeBlocking(h -> {
            Document document = Jsoup.parse(httpTools.GetContent(getGame ? gameTypeUrl : appTypesUrl, 0, 3));
            Elements elements = document.select(".container  .parent-cate>a");
            JsonArray jsonArray = new JsonArray();
            String url = null;
            String typeid;
            for (Element a : elements) {
                try {
                    url = a.attr("href");
                    if (url.equals("javascript:void(0);"))
                        continue;
                    typeid = url.substring(url.lastIndexOf("category/") + "category/".length());
                    jsonArray.add(new JsonObject().put("name", a.text()).put("url", url).put("typeid", typeid));
                } catch (Exception e) {
                    System.out.println("ErrorName:WanDouJia Find Type - " + url + " | " + e.getMessage());
                }
            }
            h.complete(jsonArray);
        }, false, typesResult);
    }

    //获取分类信息
    public void getAppTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(typesResult, false);
    }

    //获取游戏分类
    public void getGameTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(typesResult, true);
    }

    class GetAllByTypeThread {
        JsonArray allApps;
        JsonObject type;

        public GetAllByTypeThread(JsonObject type) {
            this.type = type;
        }

        public void run(Handler<AsyncResult<JsonArray>> handler) {
            Vertx.currentContext().executeBlocking(getPage -> {
                allApps = new JsonArray();
                Document d = Jsoup.parse(httpTools.GetContent(String.format(appsUrl, type.getString("typeid"), "1"), 0, 3));
                int pageCount = Integer.parseInt(d.select(".page-item:not(.next-page)").last().text().trim());

                List<Future> futures = new ArrayList<Future>();

                for (int i = 1; i <= pageCount; i++) {
                    String url = String.format(appsUrl, type.getString("typeid"), i);
                    Future future = Future.future();
                    futures.add(future);
                    Vertx.currentContext().executeBlocking(
                            (r) -> {
                                JsonArray array = new Tools.GetAllByTypeThread.GetPageAppsThread(url).GetPageApps();
                                allApps.addAll(array);
                                storeData(mongo, LocalDB.SourceType.wandoujia, (insertResult) -> {
                                    r.complete();
                                }, false, type.getString("typeid"), array);
                            },
                            false,
                            (result) -> {
                                future.complete();
                            }
                    );
                }
                CompositeFuture.all(futures).setHandler(r -> {
                    getPage.complete(allApps);
                });
            }, false, handler);
        }

        class GetPageAppsThread {
            String url;

            public GetPageAppsThread(String url) {
                this.url = url;
            }

            public JsonArray GetPageApps() {
                JsonArray apps = new JsonArray();
                Document document = Jsoup.parse(httpTools.GetContent(url, 0, 3));
                Elements elements = document.select("#j-tag-list>li");

                for (Element element : elements) {
                    apps.add(getOneDetails(element.attr("data-pn")));
                }
                return apps;
            }
        }
    }

    /*
    //获取应用根据分类
    * @type:
    * {
    *   typeid:"101"
    * }
    */
    public void getAppByType(Handler<AsyncResult<JsonArray>> resultHandler, JsonObject type) {
        GetAllByTypeThread thread = new GetAllByTypeThread(type);
        thread.run(resultHandler);
    }

    //获取游戏根据分类
    public void getGameByType(Handler<AsyncResult<JsonArray>> resultHandler, JsonObject type) {
        GetAllByTypeThread thread = new GetAllByTypeThread(type);
        thread.run(resultHandler);
    }

    //应用和游戏详情地址（含占位符）
    String appDetailsUrl = "http://www.wandoujia.com/apps/%s";

    //应用详情
    JsonObject getOneDetails(String packageName) {
        JsonObject appInfo = new JsonObject();
        try {
            //JsonObject test2 = tool2.getOneDetails(packageName);
            Document document = Jsoup.parse(httpTools.GetContent(String.format(appDetailsUrl, packageName), 0, 3));
            Element body = document.select(".container .detail-wrap").first();

            appInfo.put("appId", packageName);//包名
            appInfo.put("pkgName", packageName);//包名
            appInfo.put("apkUrl", body.select(".qr-info a").attr("href"));//下载链接

            appInfo.put("iconUrl", body.select(".app-icon img").attr("src"));

            Map<String, String> selectMap = new HashMap<>();
            selectMap.put("categoryName", ".tag-box a:eq(0)");//分类名称
            selectMap.put("appName", ".app-name .title");//应用名称
            selectMap.put("fileSizeMB", ".col-right .infos .infos-list dd:nth-child(2)");//应用大小MB
            selectMap.put("apkPublishTimeText", ".col-right .infos .infos-list dd:nth-child(8)");//更新时间 yyyy年M月d日
            //selectMap.put("versionName",".col-right .infos .infos-list dd:nth-child(10)");//版本 x.x.x.x
            selectMap.put("versionName", ".col-right .infos .infos-list dt:contains(版本) + dd");//版本 x.x.x.x
            selectMap.put("description", ".desc-info .con");//说明
            selectMap.put("newFeature", ".change-info .con");//更新内容
            selectMap.put("comment", ".editorComment .con");//简单说明


            //使用query获取值
            for (String key : selectMap.keySet()) {
                String select = selectMap.get(key);
                Elements els = body.select(select);
                if (els.size() > 0)
                    appInfo.put(key, els.first().text());
                else
                    appInfo.put(key, "");
            }

            //图片
            JsonArray appImgs = new JsonArray();
            Elements imgs = body.select(".screenshot img");
            for (Element img : imgs) {
                appImgs.add(img.attr("src"));
            }
            appInfo.put("images", appImgs);

        } catch (Exception e) {
            //System.out.println("ErrorName:wandoujia|"+packageName);
        } finally {
            return appInfo;
        }
    }

    public static boolean isValid(String strLink) {
        URL url;
        try {
            url = new URL(strLink);
            HttpURLConnection connt = (HttpURLConnection) url.openConnection();
            connt.setRequestMethod("HEAD");
            String strMessage = connt.getResponseMessage();
            if (strMessage.compareTo("Not Found") == 0) {
                return false;
            }
            connt.disconnect();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static URL url;
    private static HttpURLConnection con;
    private static int state = -1;

    public synchronized URL isConnect(String urlStr) {
        int counts = 0;
        if (urlStr == null || urlStr.length() <= 0) {
            return null;
        }
        while (counts < 5) {
            try {
                url = new URL(urlStr);
                con = (HttpURLConnection) url.openConnection();
                state = con.getResponseCode();
                System.out.println(counts + "= " + state);
                if (state == 200) {
                    System.out.println("URL可用！");
                }
                break;
            } catch (Exception ex) {
                counts++;
                System.out.println("URL不可用，连接第 " + counts + " 次");
                urlStr = null;
                continue;
            }
        }
        return url;
    }

    static boolean exists(String URLName) {

        try {

            //设置此类是否应该自动执行 HTTP 重定向（响应代码为 3xx 的请求）。

            HttpURLConnection.setFollowRedirects(false);

            //到 URL 所引用的远程对象的连接

            HttpURLConnection con = (HttpURLConnection) new URL(URLName)

                    .openConnection();

           /* 设置 URL 请求的方法， GET POST HEAD OPTIONS PUT DELETE TRACE 以上方法之一是合法的，具体取决于协议的限制。*/

            con.setRequestMethod("HEAD");

            //从 HTTP 响应消息获取状态码

            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);

        } catch (Exception e) {

            e.printStackTrace();

            return false;

        }

    }


    //更新应用库，根据本地已有的应用列表
    @Override
    public void updateLocalDB(MongoClient mongo, LocalDB.SourceType sourceType, Handler<AsyncResult<Boolean>> resultHandler) {
        new local.LocalDB().findAllPackageName(mongo, (AsyncResult<List<String>> r) -> {
            List<String> apps = r.result();
            if (apps != null) {
                List<Future> futures = new ArrayList<Future>();
                //List<Future> baoFutures = new ArrayList<Future>();
                boolean wandoujiaURL = exists("https://www.wandoujia.com/");
                //boolean yingyongbaoURL = exists("http://sj.qq.com/");
                for (int i = 0; i < apps.size(); i++) {
                    Future baoFuture = Future.future();
                    Future future = Future.future();
                    //baoFutures.add(baoFuture);
                    futures.add(future);
                    String pkg = apps.get(i);
                    Vertx.currentContext().executeBlocking(h -> {
                  //      Future yingyongbaoFuture = Future.future();
                        Future wandoujiaFuture = Future.future();
                    /*    if (yingyongbaoURL) {
                            JsonObject baoApp = tool2.getOneDetails(pkg);
                            if (baoApp != null) {
                                storeData1(mongo, LocalDB.SourceType.yingyongbao, baoApp);
                                yingyongbaoFuture.complete();
                            }
                        }
                        else {
                            yingyongbaoFuture.complete();
                        }*/
                        if (wandoujiaURL) {
                            JsonObject app = getOneDetails(pkg);
                            storeData(mongo, sourceType, insertResult -> {
                               wandoujiaFuture.complete();
                            }, app);
                        } else {
                          wandoujiaFuture.complete();
                        }
                            h.complete();

                    }, false, insertResult -> {

                        future.complete();
                    });
/*
                    JsonObject baoApp = getOneDetails(pkg);
                    if (baoApp != null) {
                        Vertx.currentContext().executeBlocking(h -> {

                            storeData1(mongo, LocalDB.SourceType.yingyongbao, insertResult -> {
                                h.complete();
                            }, baoApp);
                        }, false, insertResult -> {
                            mongo.insert("yingyongbao", baoApp, s -> {
                                if (s.succeeded()) {
                                }
                            });
                            baoFuture.complete();
                        });
                    }*/
                }

                CompositeFuture.all(futures).setHandler(allComplated -> {
                    resultHandler.handle(Future.succeededFuture());
                });
            }
        });
    }
}

