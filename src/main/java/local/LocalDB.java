package local;

import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import main.FindUpdateOptions;

import java.io.*;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by helloklf on 2016/9/8.
 */
public class LocalDB {
    //分类信息集合
    String classifyCollection = "phone580_classify";
    //应用信息集合
    String appsCollection = "phone580";

    //无更新
    public void findUpdateNotFound(Handler<AsyncResult<JsonObject>> handler) {
        handler.handle(Future.<JsonObject>succeededFuture(
                new JsonObject().put("status", "1").put("result", "0").put("msg", "未找到应用信息！").put("datas", "[]")
        ));
    }

    //更新失败
    public void findUpdateError(Handler<AsyncResult<JsonObject>> handler) {
        handler.handle(Future.<JsonObject>succeededFuture(
                new JsonObject().put("status", "-1").put("result", "0").put("msg", "Find Error").put("datas", "[]")
        ));
    }

    //查找更新（根据本地查找结果）
    void findUpdateFromHttpDBSplit(List<JsonObject> allLocalAppInfo, FindUpdateOptions options, JsonArray updates, int taskIndex) {
        int tasksCount = (allLocalAppInfo.size() / 10) + (allLocalAppInfo.size() % 10 == 0 ? 0 : 1);
        if (taskIndex < tasksCount) {
            int end = (taskIndex + 1) * 10;
            end = end > allLocalAppInfo.size() ? allLocalAppInfo.size() : end;
            List<JsonObject> localAppInfo = allLocalAppInfo.subList(taskIndex * 10, end);

            int st = options.getSourceType();
            ArrayList<Future> futures = new ArrayList<>();
            http.LocalDB httpDB = new http.LocalDB(options.mongoClient);
            Lock lock = new ReentrantLock();

            for (int i = 0; i < localAppInfo.size(); i++) {
                int index = i;
                Future future = Future.future();
                futures.add(future);
                JsonObject item = localAppInfo.get(index);
                JsonObject queryInfo = options.GetQueryInfo(item);
                String vName = item.getString("versionName");

                Handler<AsyncResult<JsonArray>> h = new Handler<AsyncResult<JsonArray>>() {
                    @Override
                    public void handle(AsyncResult<JsonArray> httpResult) {

                        JsonArray result;
                        if (httpResult.succeeded() && (result = httpResult.result()) != null && result.size() > 0) {
                            lock.lock();
                            try{

                                for (int i = 0; i < result.size(); i++) {
                                    JsonObject resultItem = result.getJsonObject(i);
                                    int compare = main.Tools.versionNameCompare(resultItem.getString("versionName"), vName);
                                    if (compare == 0) {
                                        JsonObject data = resultItem.getJsonObject("data");
                                        String versionCode = data.getString("versionCode","");
                                        try {
                                            if (versionCode != null && !versionCode.equals(""))
                                                compare = main.Tools.IsVersionCode(versionCode, item.getString("versionCode"));
                                        }
                                        catch (Exception ex){  }
                                    }
                                    if (compare == 1) { //版本比较
                                        resultItem.put("local", item).put("versionCompare", compare);
                                        updates.add(resultItem);
                                    } else if (compare == -2 && options.unknownVersion) {//是否显示未知版本
                                        resultItem.put("local", item).put("versionCompare", compare);
                                        updates.add(resultItem);
                                    } else if ((compare == 0 || compare == -1) && options.localVersion) {
                                        resultItem.put("local", item).put("versionCompare", compare);
                                        updates.add(resultItem);
                                    }
                                }
                            }
                            catch (Exception exxx){
                                System.out.print(exxx.getMessage());
                            }
                            lock.unlock();
                        } else if (options.localVersion) {
                            updates.add(new JsonObject().put("local", item).put("versionCompare", "-1"));
                        }

                        future.complete();

                    }
                };

                /*
                if (st > -1)
                    httpDB.findOne(queryInfo, http.LocalDB.SourceType.values()[st], options.resultFields, h);
                else
                    httpDB.findOne(queryInfo, options.resultFields, h);
                */
                httpDB.findOne(queryInfo, options.resultFields, h);
            }

            final int nextTaskIndex = taskIndex + 1;
            CompositeFuture.all(futures).setHandler(all -> {
                findUpdateFromHttpDBSplit(allLocalAppInfo, options, updates, nextTaskIndex);
            });
        } else {
            int size = updates.size();
            if (size > 0) {
                options.handler.handle(Future.<JsonObject>succeededFuture(
                        new JsonObject()
                                .put("status", "1")
                                .put("result", updates.size())
                                .put("msg", "搜索成功！")
                                .put("datas", updates)
                ));
            } else {
                options.handler.handle(Future.<JsonObject>succeededFuture(
                        new JsonObject().put("status", "1").put("result", "0").put("msg", "未找到可用更新！").put("datas", "[]")
                ));
            }
        }
    }

    //查找更新（根据本地查找结果）
    void findUpdateFromHttpDB(List<JsonObject> localAppInfo, FindUpdateOptions options) {
        JsonArray updates = new JsonArray();
        //避免大量并发导致等待线程超出限制，优化查询效率，当要执行的查询数超过10，拆分操作
        findUpdateFromHttpDBSplit(localAppInfo, options, updates, 0);
    }

    JsonArray classifyGroup(JsonArray jsonArray) {
        JsonArray array = new JsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            array.add(
                    new JsonObject()
                            .put("value", jsonArray.getJsonObject(i).getString("typeid"))
                            .put("label", jsonArray.getJsonObject(i).getString("typeName"))
                            .put("group", jsonArray.getJsonObject(i).getString("ptypeid"))
            );
        }
        return array;
    }

    //检查是否是某个类型的子类（用于分类搜索结果过滤）
    boolean classifOfBase(List<JsonObject> array, JsonObject itemType, JsonObject baseType) {
        String ptypeid = itemType.getString("ptypeid");
        String typeid = itemType.getString("typeid");
        ptypeid = ptypeid == null ? "" : ptypeid.trim();

        if ((typeid.equals("") || typeid.equals("0"))) {
            return false;
        } else {
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getString("typeid").equals(ptypeid)) {
                    if (array.get(i).getString("typeid") == baseType.getString("typeid"))
                        return true;
                    else
                        return classifOfBase(array, array.get(i), baseType);
                }
            }
        }
        return false;
    }

    //检查是否是某个类型的子类（用于分类搜索结果过滤）
    boolean classifyIsClassify(List<JsonObject> array, String typeid, String baseType) {
        if (typeid.equals(baseType)) {
            return true;
        } else {

            JsonObject itemType = null;
            for (int index = 0; index < array.size(); index++) {
                if ((itemType = array.get(index)).getString("typeid").equals(typeid)) {
                    for (int i = 0; i < array.size(); i++) {
                        if (array.get(i).getString("typeid").equals(baseType)) {
                            return classifOfBase(array, itemType, array.get(i));
                        }
                    }
                }
            }
            return false;
        }
    }

    //获取分类信息
    public void findClassify(MongoClient mongo, Handler<AsyncResult<JsonArray>> handler) {
        mongo.find(classifyCollection, new JsonObject(), queryResult -> {
            if (queryResult.succeeded())
                handler.handle(Future.<JsonArray>succeededFuture(
                        classifyGroup(
                                main.Tools.<JsonObject>ListToJsonArray(queryResult.result())
                        )
                ));
            else
                handler.handle(Future.<JsonArray>failedFuture("查询失败！"));
        });
    }

    //获取所有本地应用的包名
    public void findAllPackageName(MongoClient mongo, Handler<AsyncResult<List<String>>> handler) {
        FindOptions findOptions = new FindOptions();
        findOptions.setFields(new JsonObject().put("packageName", ""));
        mongo.findWithOptions(appsCollection, new JsonObject(), findOptions, result -> {
            List<JsonObject> arr = result.result();
            List<String> packages = new ArrayList<String>();
            if (arr != null) {
                for (int i = 0; i < arr.size(); i++) {
                    packages.add(arr.get(i).getString("packageName"));
                }
            }
            handler.handle(Future.succeededFuture(packages));
        });
    }


    //检查更新
    public void findUpdate(main.FindUpdateOptions options) {
        options.mongoClient.findWithOptions(appsCollection, options.queryInfo, options.findOptions, localResult -> {
            if (localResult.succeeded()) {
                List<JsonObject> result = localResult.result();
                List<JsonObject> items = new ArrayList<JsonObject>();
                if (options.classify != null) {
                    options.mongoClient.find(classifyCollection, new JsonObject(), queryResult -> {
                        List<JsonObject> classIfys = queryResult.result();
                        for (JsonObject item : result) {
                            if (classifyIsClassify(classIfys, item.getString("appClassify"), options.classify))
                                items.add(item);
                        }
                        findUpdateFromHttpDB(items, options);
                    });
                } else {
                    findUpdateFromHttpDB(result, options);
                }
            } else
                findUpdateError(options.handler);
        });
    }

    //更新本地数据库
    public void updateDB(JsonObject configs, MongoClient mongo, Handler<AsyncResult<Void>> handler) {
        Vertx.currentContext().executeBlocking(h -> {
            updateClassify(configs, mongo, hr -> {
                updateAppinfo(configs, mongo, handler);


            });
        }, false, r -> {
        });
    }



    void updateClassify(JsonObject configs, MongoClient mongo, Handler<AsyncResult<Void>> handler) {
        String baseUrl = configs.getString("Ftp_IP", "183.232.28.156");
        String dic = configs.getString("Ftp_Dir", "support");
        String classify = configs.getString("Ftp_AppClassify", "product_classify.txt");
        String uid = configs.getString("Ftp_UID", "pachong");
        String pwd = configs.getString("Ftp_PWD", "pc123*");

        //10|网络社区|1|1
        if (new FtpTools().downFile(baseUrl, 21, uid, pwd, dic, classify, "./LocalDB/", 0, 5)) {
            mongo.dropCollection(classifyCollection, dropResult -> {
                if (dropResult.succeeded()) {

                    File appsFile = new File("./LocalDB/" + classify);
                    ArrayList<Future> futures = new ArrayList<Future>();
                    try {
                        BufferedReader reader;
                        reader = new BufferedReader(new InputStreamReader(new FileInputStream(appsFile), "UTF-8"));
                        String row = null;
                        while ((row = reader.readLine()) != null) {
                            String[] cols = row.trim().split("\\|");
                            Future future = Future.future();
                            futures.add(future);

                            try {
                                JsonObject appInfo;
                                appInfo = new JsonObject()
                                        .put("typeid", cols[0])
                                        .put("typeName", cols[1])
                                        .put("ptypeid", cols[2])
                                        .put("id", cols[3]);

                                mongo.insert(classifyCollection, appInfo, insertResult -> {
                                    future.complete();
                                });
                            } catch (Exception ex) {
                                future.complete();
                                System.out.println("Invalid AppClassify Info:" + row);
                            }
                        }
                    } catch (Exception ex) {
                        System.out.println("\n\n>>> { LocalDB(FTP):Insert AppClassify HistoryLog Error! }\n");
                        handler.handle(Future.failedFuture("更新本地分类失败！"));
                    } finally {
                        CompositeFuture.all(futures).setHandler(r -> {
                            System.out.println("\n\n>>> { LocalDB(FTP):AppClassify HistoryLog Updated! }\n");
                            handler.handle(Future.succeededFuture());
                        });
                    }

                } else {
                    System.out.println("\n\n>>> { LocalDB(FTP):Drop AppClassify HistoryLog Error! }\n");
                    handler.handle(Future.failedFuture("更新本地分类失败！"));
                }
            });
        } else {
            System.out.println("\n\n>>> { LocalDB(FTP):Download LocalAppClassify HistoryLog Error! }\n");
            handler.handle(Future.failedFuture("更新本地分类失败！"));
        }
    }

    void updateAppinfo(JsonObject configs, MongoClient mongo, Handler<AsyncResult<Void>> handler) {
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        try {
            now = f.parse(new SimpleDateFormat("yyyyMMdd").format(now));
        } catch (Exception ex) {
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(now);
        gc.add(Calendar.DATE, -1);
        now = gc.getTime();

        String baseUrl = configs.getString("Ftp_IP", "183.232.28.156");
        String dic = configs.getString("Ftp_Dir", "support");
        String apps = configs.getString("Ftp_AppsFile", "product_%date.txt").replace("%date", f.format(now));
        String uid = configs.getString("Ftp_UID", "pachong");
        String pwd = configs.getString("Ftp_PWD", "pc123*");

        //562|美图秀秀|32|android|4.0.0|400|com.mt.mtxx.mtxx
        if (new FtpTools().downFile(baseUrl, 21, uid, pwd, dic, apps, "./LocalDB/", 0, 5)) {

            mongo.dropCollection(appsCollection, dropResult -> {
                if (dropResult.succeeded()) {
                    File appsFile = new File("./LocalDB/" + apps);
                    ArrayList<Future> futures = new ArrayList<Future>();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(appsFile), "UTF-8"));
                        String row = null;
                        while ((row = reader.readLine()) != null) {
                            String[] cols = row.trim().split("\\|");
                            Future future = Future.future();
                            futures.add(future);

                            try {
                                JsonObject appInfo;
                                appInfo = new JsonObject()
                                        .put("appid", cols[0])
                                        .put("appName", cols[1])
                                        .put("appClassify", cols[2])
                                        .put("platform", cols[3]);

                                appInfo.put("versionName", (cols.length > 4) ? cols[4] : "");
                                appInfo.put("versionCode", (cols.length > 5) ? cols[5] : "");
                                appInfo.put("packageName", (cols.length > 6) ? cols[6] : "");

                                mongo.insert(appsCollection, appInfo, insertResult -> {
                                    future.complete();
                                });
                            } catch (Exception ex) {
                                System.out.println("Invalid App Info:" + row);
                            }
                        }
                    } catch (Exception ex) {
                        System.out.println("\n\n>>> { LocalDB(FTP):Insert App HistoryLog Error! }\n");
                        handler.handle(Future.failedFuture("更新本地应用失败！"));
                    } finally {
                        CompositeFuture.all(futures).setHandler(r -> {
                            System.out.println("\n\n>>> { LocalDB(FTP):App HistoryLog Complated! }\n");
                            handler.handle(Future.succeededFuture());
                        });
                    }

                } else {
                    System.out.println("\n\n>>> { LocalDB(FTP):App Drop HistoryLog Error! }\n");
                    handler.handle(Future.failedFuture("更新本地应用失败！"));
                }
            });
        } else {
            System.out.println("\n\n>>> { LocalDB(FTP):Download LocalAppInfo HistoryLog Error! }\n");
            handler.handle(Future.failedFuture("更新本地应用失败！"));
        }
    }
}
