package local;

import http.LocalDB;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by helloklf on 2016/8/25.
 */
public interface StoreInterfaceAsync {

    void getAppTypes(Handler<AsyncResult<JsonArray>> typesResult);
    void getGameTypes(Handler<AsyncResult<JsonArray>> typesResult);
    void getAppByType(Handler<AsyncResult<JsonArray>> appsResult, JsonObject type);
    void getGameByType(Handler<AsyncResult<JsonArray>> gamesResult, JsonObject type);
    //void getAllApps(Handler<AsyncResult<JsonArray>> appsResult);
    //void getAllGames(Handler<AsyncResult<JsonArray>> gamesResult);
}
