package local.phone580;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import local.StoreInterfaceAsync;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by helloklf on 2016/9/8.
 */
public class Tools implements StoreInterfaceAsync {
    MongoClient mongo;
    public Tools(MongoClient mongo){
        this.mongo = mongo;
    }
    public void getTypes(Boolean isGame,Handler<AsyncResult<JsonArray>> typesResult) {
        mongo.find(
                "phone580_classify",
                new JsonObject().put("pid",isGame?"2":"1"),
                result->{
                    JsonArray array = new JsonArray();
                    List<JsonObject> items = result.result();
                    if(result.succeeded()&&items!=null){
                        for (JsonObject item:items){
                            array.add(item);
                        }
                    }
                    typesResult.handle(Future.<JsonArray>succeededFuture(array));
                }
        );
    }

    @Override
    public void getAppTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(false,typesResult);
    }

    @Override
    public void getGameTypes(Handler<AsyncResult<JsonArray>> typesResult) {
        getTypes(true,typesResult);
    }

    @Override
    public void getAppByType(Handler<AsyncResult<JsonArray>> appsResult, JsonObject type) {

    }

    @Override
    public void getGameByType(Handler<AsyncResult<JsonArray>> gamesResult, JsonObject type) {

    }
}
