﻿if (window.$ != null) {
    $.fn.extend({
        Active: function (data) {
            if (data == false) {
                this.removeClass("Active");
            }
            else {
                this.addClass("Active");
            }
            return this;
        },
        UnActive: function (data) {
            if (data == false) {
                this.addClass("Active");
            }
            else {
                this.removeClass("Active");
            }
            return this;
        },
        ToggleActive: function (data) {
            this.toggleClass("Active");
            return this;
        },

        Where: function (fun) {
            if (fun) {
                var stack = [];
                for (var i = 0; i < this.size(); i++) {
                    if (fun(this[i])) {
                        stack.push(this[i]);
                    }
                }
                return $(stack);
            }
            return this;
        }
    });
}


//获取当天是星期几
function Date_GetWeek(year, month, day) {
    var datas = parseInt((new Date(year, month - 1, day) - new Date(1979, 0, 1)) / 3600000 / 24);
    var v = Math.abs(((datas) % 7)) + 1; //星期几？1为星期一，7为星期日
    return v;
}

//获取某年某月有多少天
function Date_GetMonthDayCount(year, month) {
    var JsDataBigMonth = [1, 3, 5, 7, 8, 10, 12]; //大月
    if (JsDataBigMonth.indexOf(month) > -1) 
        return 31;
    else if (month == 2) {
        var leap = YearIsLeap(year);
        if (leap)
            return 29;
        else
            return 28;
    }
    else {
        return 30;
    }
}

//判断某年是不是闰年
YearIsLeap = function (year) {
    return ((year % 4 == 0 || year % 400 == 0) && year % 100 != 0);
};
//获取日期的年份是否是闰年
Date.prototype.IsLeap = function () {
    return YearIsLeap( this.getYear() );
};


//valuePath:键
getPathValue = function (valuePath,showNullError) {
    try{
        var index = valuePath.indexOf(".");
        if(index>0){
            var pathFirst = valuePath.substring(0,index);
            var val = this[pathFirst];
            return getPathValue.call(val,valuePath.substring(index+1),showNullError);
        }
        else {
            var val = this[valuePath];
            return val;
        }
    }
    catch (e){
        if(showNullError) {
            console.log("Object.prototype.getValue Error\nvaluePath:" + valuePath);
            console.log("this:");
            console.log(this);
        }
        return "";
    }
}
/*
 例如：
 var obj = {
 datas:[
 {a:0,b:1}
 ],
 data:{
 name:"cc",
 age:12
 }
 };
 var age = obj.getValue("data.name");
 var a = obj.getValue("datas.0.a");
 */

//文本框输入验证
$(function () {
    InputValid = function (base) {
        var formItems = $("select[data-require],textarea[data-require],input[data-require],select[data-require]:not([multiple])", $(base));
        var passCount = 0;
        formItems.each(function (index, item) {
            var val = item.value;
            if (!IsEmpty(val)) {
                item.focus();
                passCount++;
                $(item).removeClass("ValidError");
            }
            else {
                $(item).addClass("ValidError");
            }
        });
        return passCount == formItems.size();
    };

    $("input[maxlength],textarea[maxlength]").on("input", function (e) {
        var input = $(e.delegateTarget);
        var val = input.val();
        var maxLen = parseInt(input.attr("maxlength"));
        if (val.length > maxLen)  input.val(val.substring(0,maxLen));
    });
    $("input[type='datetime']").on("input", function (e) {
        var input = $(e.delegateTarget);
        var val = input.val();

        input.val(val);
    });
    $("input[type='number']").on("change", function (e) {
        var input = $(e.delegateTarget);
        var val = parseFloat(input.val());
        if (isNaN(val)) val = 0;

        var min = parseFloat(input.attr("min"));
        var max = parseFloat(input.attr("max"));

        if (!isNaN(max) && val > max) input.val(max);
        else if (!isNaN(min) && val < min) input.val(min);
        else input.val(val);
    });
});

//将一维Json对象转换为url格式参数字符串
function JsonToUrlParaString(data) {
    var paras = "";
    if (data != null && (data instanceof Object)) {
        for (var key in data) {
            paras += (key + "=" + encodeURIComponent( data[key] ) + "&");
        }
    }
    return paras;
}

//将url格式参数字符串转换为一维Json对象
function UrlParaStringToJson(urlParas) {
    var obj = {};
    try {
        name = name.toLowerCase();
        var href = IsEmpty(urlParas) ? "" : urlParas;
        var paras = href.substring(href.lastIndexOf("?") + 1);
        var paraArr = paras.split("&");
        for (var i = 0; i < paraArr.length; i++) {
            var key = paraArr[i].substring(0, paraArr[i].indexOf("="));
            var val = paraArr[i].substring(paraArr[i].indexOf("=") + 1);
            obj[key] = val;
        }
    }
    catch (e) { }
    return obj;
}

//根据Class获取DOM元素（IE9+）
function $class(className) {
    return document.getElementsByClassName(className);
}
//根据ID获取DOM元素
function $id(id) {
    return document.getElementById(id);
}
//创建节点
function $newe(type) {
    return document.createElement(type);
}
//为DOM元素创建事件监听（IE5+）
function $addEvent(dom, event, fun) {
    if (dom) {
        if (dom.addEventListener) {
            dom.addEventListener(event, fun);
        }
        else
            dom.attachEvent("on" + event, function () { alert(1) });
    }
    else
        console.log("指定的Dom元素为空，无法绑定事件！");
}


/*
 * urlAndFun    ：Url和方法名，如 login.aspx/UidCheck
 * datas           : 提交的表单内容
 * onSuccess    ：请求成功后的回调函数
 * onError         ：请求失败后的回调函数
 * onCompleted :请求结束后的回调函数，不管是否成功
 */
function WebMethodRequest(urlAndFun, datas, onSuccess, onError, onCompleted) {
    datas = datas == null ? "" : datas;
    $.ajax({
        type: "Post",//要用post方式     
        url: urlAndFun, //方法所在页面和方法名     
        data: (typeof (datas) == "string") ? datas : JSON.stringify(datas),//要转化为字符串格式
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            if (onSuccess)
                onSuccess(r.d);
        },
        error: onError,
        complete: function (a, b) {
            if (onCompleted)
                onCompleted(a, b);
        }
    });
}



//是否是PC设备
function IsPC() {
    var userAgentInfo = navigator.userAgent;
    var Agents = new Array("Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod");
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) { flag = false; break; }
    }
    return flag;
}

//拓展文字替换功能，替换全部匹配的文本
String.prototype.replaceAll = function (oldVal, newVal) {
    var reg = new RegExp(oldVal, "gi");
    return this.replace(reg, newVal);
};
//判断值是否为空
IsEmpty = function (val) {
    return !(val != null && val.trim() != "" && val.trim() != "null" && val.trim() != "undefined");
}
//判断文本是否为空
String.prototype.isEmpty = function () {
    return IsEmpty(this);
};


//为数组拓展分组方法
Array.prototype.groupBy = function (fun) {
    var groups = {};
    for (var i = 0; i < this.length; i++) {
        var key = fun(this[i]);
        if (groups[key] == null) {
            groups[key] = new Array();
        }
        groups[key].push(this[i]);
    }
    return groups;
};

//转成树结构(topPval:顶级分类的pkey值)
Array.prototype.toTree = function (key,pkey,topPval) {
    var tree = {};
    var topLevels = this.where(function (item) {
        return item[pkey] == topPval;
    });
    var getChildLevel = function (array,groupKey){
        var childLeves = array.where(function (c) {
           return c[pkey] == groupKey;
        });
        if(childLeves.length>0) {
            for (var i = 0; i < childLeves.length; i++) {
                childLeves[i]["datas"] = arguments.callee(array, childLeves[i][key]);
            }
        }
        return childLeves;
    }

    for (var i = 0; i < topLevels.length; i++) {
        topLevels[i]["datas"] = getChildLevel(this,topLevels[i][key]);
    }
    return topLevels;
};

//条件过滤
Array.prototype.where = function (fun) {
    var stack = new Array();
    for (var i = 0; i < this.length; i++) {
        var row = this[i];
        if (fun(row)) {
            stack.push(row);
        }
    }
    return stack;
};

//如果val为空，则替换为defaultVal
ReplaceEmptyString = function (val, defaultVal) {
    return (val != null && val.trim() != "" && val.trim() != "null" && val.trim() != "undefined") ? val : defaultVal;
}

//获取页面路径，不包含域名（仅支持常规格式，MVC不兼容）
//hasPara：是否包含参数
function PageUrl(hasPara) {
    var fullUrl = location.href;
    var pageUrl = fullUrl.substring(fullUrl.lastIndexOf("://") + 3);
    pageUrl = pageUrl.substring(pageUrl.indexOf("/"));

    if (hasPara)
        return pageUrl;

    if (pageUrl.indexOf("?") > -1)
        pageUrl = pageUrl.substring(0, pageUrl.indexOf("?"))
    if (pageUrl.indexOf("#") > -1)
        pageUrl = pageUrl.substring(0, pageUrl.indexOf("?"));

    return pageUrl;
}

//获取页面URL上的参数
function UrlPara(name) {
    try {
        name = name.toLowerCase();
        var href = window.location.href;
        var paras = href.substring(href.lastIndexOf("?") + 1);
        var paraArr = paras.split("&");
        for (var i = 0; i < paraArr.length; i++) {
            var key = paraArr[i].substring(0, paraArr[i].indexOf("="));
            var val = paraArr[i].substring(paraArr[i].indexOf("=") + 1);
            if (key.toLowerCase() == name)
                return val;
        }
    }
    catch (e) {
        return "";
    }
}

Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

/** * 对Date的扩展，将 Date 转化为指定格式的String * 月(M)、日(d)、12小时(h)、24小时(H)、分(m)、秒(s)、周(E)、季度(q)
    可以用 1-2 个占位符 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) * eg: * (new
    Date()).pattern("yyyy-MM-dd hh:mm:ss.S")==> 2006-07-02 08:09:04.423      
 * (new Date()).pattern("yyyy-MM-dd E HH:mm:ss") ==> 2009-03-10 二 20:09:04      
 * (new Date()).pattern("yyyy-MM-dd EE hh:mm:ss") ==> 2009-03-10 周二 08:09:04      
 * (new Date()).pattern("yyyy-MM-dd EEE hh:mm:ss") ==> 2009-03-10 星期二 08:09:04      
 * (new Date()).pattern("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18      
 */
Date.prototype.pattern = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份         
        "d+": this.getDate(), //日         
        "h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //小时         
        "H+": this.getHours(), //小时         
        "m+": this.getMinutes(), //分         
        "s+": this.getSeconds(), //秒         
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度         
        "S": this.getMilliseconds() //毫秒         
    };
    var week = {
        "0": "日",
        "1": "一",
        "2": "二",
        "3": "三",
        "4": "四",
        "5": "五",
        "6": "六"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "星期" : "周") : "") + week[this.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}


function parseDeteTime(text) {
    var dt = eval('new ' + (text.replace(/\//g, '')));
    return dt;
};
function parseDete(text) {
    var dt = eval('new ' + (text.replace(/\//g, '')));
    return dt.Format("yyyy-MM-dd");
};
function parseTime(text) {
    var dt = eval('new ' + (text.replace(/\//g, '')));
    return dt.Format("HH:mm:ss");
};

var historyStack = new Array();
//执行windows.history.push操作
//onBack：当点击页面返回时的操作
//onBack参数可以是方法名、对象、脚本字符串
function OAUI_PushState(onBack, title) {
    historyStack.push(onBack);
    if (onBack.name && window[onBack.name]) {
        window.history.pushState(onBack.name, title);
    }
    else {
        window.history.pushState("window.history.pushState =" + onBack.toString(), title);
    }
}
function OAUI_StateBack() {
    try {
        var onBack = historyStack.pop();
        if (onBack != null) {
            if (onBack instanceof Function) {
                onBack();
            }
            else {
                var fun = eval(onBack);
                fun();
            }
        }
    } catch (e) {
        Tips("页面状态管理异常：" + e.message);
    }
}
function OAUI_StateClear() {
    console.log(historyStack.length + "个状态已清理");
    window.history.go(-historyStack.length);
}
$addEvent(window, "popstate", OAUI_StateBack);//绑定历史纪录返回事件
//$addEvent(window, "unload", OAUI_StateClear);//绑定历史纪录卸载事件（由于存在bug，暂时取消该功能）



//中止事件
CancelEvent = function (e) {
    if (window.event) {
        window.event.returnValue = false;
        window.event.cancelBubble = true;
    }
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();
    return false;
};




//block：监听滑动事件的元素
//pixel：滑动多少像素才出发滑动
//top - bottom：向各个方向滑动时所调用的方法
//stopDefaultEvent：终止默认的事件？
//onlyTouch：是否只监听触摸事件
function InitTouchSlide(block, pixel, top, right, bottom, left, stopDefaultEvent, onlyTouch) {
    var baseItem = $(block);
    window.OAUI_SlideEventListeners = false;
    var speed = pixel ? pixel : 30; //设置滑动速度
    var startX = 0;
    var startY = 0;

    //开始滑动
    var onSlideStart = function (point) {
        OAUI_SlideEventListeners = true;
        //保存起始触摸位置
        startX = point.X;
        startY = point.Y;
    }
    //滑动结束
    var onSlideEnd = function (point) {
        OAUI_SlideEventListeners = false;
    }
    //正在滑动
    var onSlider = function (point, e) {
        if (OAUI_SlideEventListeners == true) {
            var val = 0;
            if ((Math.abs(point.X - startX) > Math.abs(point.Y - startY)) && (left || right)) {
                if (point.X - startX >= speed) val = 2;//向右滑
                else if (point.X - startX <= -speed) val = 4;//向左滑
            }
            else {
                if (point.Y - startY >= speed) val = 3;//向下滑动
                else if (point.Y - startY <= -speed) val = 1;//向上滑动
            }

            if (val > 0) onSlideEnd(point); //完成滑动操作
            var offset = { x: point.X - startX, y: point.Y - startY };
            var rval = false;
            switch (val) {
                case 1: {
                    if (top) rval = top(offset);
                    else rval = !stopDefaultEvent;
                    break;
                }
                case 2: {
                    if (right) rval = right(offset);
                    else rval = !stopDefaultEvent;
                    break;
                }
                case 3: {
                    if (bottom) rval = bottom(offset);
                    else rval = !stopDefaultEvent;
                    break;
                }
                case 4: {
                    if (left) rval = left(offset);
                    else rval = !stopDefaultEvent;
                    break;
                }
                default: {
                    break;
                }
            }
            if (rval == false) {
                e.returnValue = false;
                if (stopDefaultEvent) {
                    if (e.stopPropagation) e.stopPropagation();
                    if (e.preventDefault) e.preventDefault();
                }
            }
        }
    }

    if (!onlyTouch) {
        baseItem.on("mousedown", function (e) {
            if (e.target.value != null) onSlideEnd()
            else onSlideStart({ X: e.clientX, Y: e.clientY }, e);
        })
    .on("mousemove", function (e) {
        if (OAUI_SlideEventListeners) {
            return onSlider({ X: e.clientX, Y: e.clientY }, e);
        }
    })
        .on("mouseup", function (e) {
            onSlideEnd({ X: e.clientX, Y: e.clientY }, e);
        })
        .on("mouseout", function (e) {
            if ($(e.target) == baseItem)
                onSlideEnd({ X: e.clientX, Y: e.clientY }, e);
        })
        .on("mousewheel", function (e) {
            onSlideStart({ X: 0, Y: 0 });
            onSlider({ X: 0, Y: event.wheelDelta }, e);
            onSlideEnd({ X: startX, Y: startY + event.wheelDelta }, e);
        })
        .on("DOMMouseScroll", function (e) {
            onSlideStart({ X: 0, Y: 0 });
            onSlider({ X: 0, Y: -(e.originalEvent.detail * 40) }, e)
            onSlideEnd({ X: 0, Y: e.detail }, e);
        })
    }
    baseItem.on("touchstart", function (e) {
        var point0 = event.changedTouches[0];
        onSlideStart({ X: point0.clientX, Y: point0.clientY }, e);
    })
    .on("touchmove", function (e) {
        //if (event.preventDefault)
        //    event.preventDefault();
        var point0 = event.changedTouches[0];
        onSlider({ X: point0.clientX, Y: point0.clientY }, e);
    })
    .on("touchend", function (e) {
        var point0 = event.changedTouches[0];
        onSlideEnd({ X: point0.clientX, Y: point0.clientY }, e);
    })
}


//element：监听滑动事件的元素
//onTop：向上滑动时调用
//onBottom：向下滑动时调用
//onEnd：滑动到底部时调用
Init_ScrollEvent = function (element, onTop, onBottom, onEnd) {
    var lastOffset = 0;
    $addEvent($(element)[0], "scroll", function (e) {
        var item = e.target;
        var top = item.scrollTop;
        if (lastOffset < top) {
            if (onBottom)
                onBottom();
            if (top >= (item.scrollHeight - item.clientHeight)) {
                if (onEnd)
                    onEnd();
            }
        }
        else {
            if (onTop)
                onTop();
        }
        lastOffset = top;
    });
}




// #region Json到Html

///使用Json对象填充表单
///jObject：表单值对象{key:"value"}
///formSelector：表单选择器
///checkboxValSplit：复选框选中值分隔符
UsingJsonFillForm = function (jObject, formSelector, checkboxValSplit) {
    //console.log(jObject);
    var form = formSelector ? $(formSelector) : $(body);
    checkboxValSplit = checkboxValSplit ? checkboxValSplit : ","; //默认的分隔符为“,”

    var resetBtn = $("[type=reset]", formSelector);
    if (resetBtn.size() > 0) resetBtn[0].click(); //清空原来的表单

    var reg = new RegExp("<br>|<br/>|<br >|<br />", "gi"); //用于替换br

    var radios = $("input[type=radio]",form);
    var checkboxs = $("input[type=checkbox]",form);
    var selects = $("select", form);
    var inputs = $("input:not([type=radio]):not([type=checkbox]),textarea", form);

    for (var key in jObject) {
        try {
            var radio = radios.Where(function (_currentElement) {
                return _currentElement.name.toLowerCase() == key.toLowerCase();
            });//单选框
            var checkbox = checkboxs.Where(function (_currentElement) {
                return _currentElement.name.toLowerCase() == key.toLowerCase();
            });//复选框
            var select = selects.Where(function (_currentElement) {
                return _currentElement.name.toLowerCase() == key.toLowerCase();
            });//下拉框

            if (radio.size() > 0) {
                var r = radio.filter("[value='" + jObject[key] + "']");
                if (r.size() > 0) r[0].checked = true;
            }
            else if (checkbox.size() > 0) {
                var valArr = jObject[key].split(checkboxValSplit);
                valArr.forEach(function (val, index) {
                    var c = checkbox.filter("[value='" + jObject[key] + "']");
                    if (c.size() > 0) c[0].checked = true;
                });
            }
            else if (select.size() > 0) {
                select.val(jObject[key]);
            }
            else {
                var v = jObject[key];
                v = v != null ? v : "";
                if (v.replace) {
                    v = v.replace(reg, "\r\n");
                }
                inputs.Where(function (_currentElement) {
                    return _currentElement.name.toLowerCase() == key.toLowerCase();
                }).val(v);
            }
        }
        catch (e) {
            console.log("表单赋值错误（UsingJsonFillForm）：" + e.message + "（错误数据：" + key + "：" + jObject[key] + "，到>>" + formSelector + "）");
        }
    }
};

//使用Json填充多选列表框
//select：多选列表
//vals：值集合，可以使["",""]或"1,2"格式
//splitChar：值分割符号（当vals为字符串时有用），默认为,
UsingValFillMultiple = function (select,vals, splitChar) {
    if (!(vals instanceof Array)) {
        vals = vals.split(splitChar == null ? "," : splitChar);
    }
    select = $(select);
    var options = $("option", select);
    options.each(function (i, item) {
        item.selected = false;
    });
    for (var i = 0; i < vals.length; i++) {
        var val = vals[i];
       var valid = options.Where(function (item) {
            return item.value.toLowerCase() == val.toLowerCase();
       });
       valid.each(function (i, item) {
           item.selected = true;
       });
    }
};

///使用Json对象填充文本框
///jObject：Json数据对象
///name：key
///base：表单元素搜索范围
UsingJsonFillInput = function (jObject, name, base) {
    $("[name=" + name + "]", base ? $(base) : null).val(jObject[name]);
};

//使用值设置select选中项
UsingValueSetSelect = function (val, select) {
    var list = $(select);
    var o = $("option[value='" + val + "']", list);
    if (o.size() > 0)
        o[0].selected = true;
    else {
        var setItem = 0;
        $("option", list).each(function (index, item) {
            if (item.value == val){
                item.selected = true;
                setItem++;
            }
        });
        if (setItem < 1) {
            $("option", list).each(function (index, item) {
                if ($(item).text() == val)
                    item.selected = true;
            });
        }
    }
}

//使用值字符串设置标签（OAUI_CheckItem）选中状态
//vals：值字符串
//selector：标签搜索范围
//split：值分隔符（默认为","）
UsingValStringSetTagChecked = function (vals, selector, split) {
    var varr = vals.split(split ? split : ",");
    var items = $(".OAUI_CheckItem", selector ? $(selector) : null).removeClass("Active");
    for (var i = 0; i < varr.length; i++) {
        items.filter("[data-val='" + varr[i] + "']").addClass("Active");
    }
};

//使用Json设置标签（OAUI_CheckItem）选中状态
//jObject：值对象
//selector：标签搜索范围
//fullMatch：是否要求完全匹配（为false则将 obj.val 与 data-val 比较）
UsingJsonSetTagChecked = function (json, selector, fullMatch) {
    var items = $(".OAUI_CheckItem", selector ? $(selector) : null).removeClass("Active");
    for (var i = 0; i < json.length; i++) {
        var obj = json[i];
        if (fullMatch) {
            var f = "";
            for (var key in obj) {
                f += ("[data-" + key + "='" + obj[key] + "']");
            }
            items.filter(f).addClass("Active");
        }
        else {
            items.filter("[data-val='" + obj.val + "']").addClass("Active");
        }
    }
};
/*
    fullMatch 全匹配是什么？
    如 { val:'1', type:'A'} 和 <span class='OAUI_CheckItem' data-val='1' data-type='A'></span>
    应确保 Object 各个值都在 OAUI_CheckItem 元素上有符合的值（键前面+“data-”）
*/


///使用模板创建节点
///templateSelector：模板选择器
///jObject:Json对象
///appendTo：新节点添加到
///newNodeId：新节点id
UsingTemplateCreateNode = function (templateSelector, jObject, appendTo, newNodeId) {
    var node = $(templateSelector).html();
    for (var key in jObject) {
        var reg = new RegExp("{" + key + "}", "gi");
        var val = jObject[key];
        val = val ? val : "";
        if (/\/Date\([0-9]*\)/.test(val)) {
            val = parseDeteTime(val).Format("yyyy-MM-dd hh:mm:ss");
        }
        node = node.replace(reg, val);
    }
    var $node = $(node);
    if (newNodeId) $node.attr("id", newNodeId);
    if (appendTo) $(appendTo).append($node);
    return $node;
}
/*Demo
<div class="OAUI_DialogMask Active">
    <div class="PromptPanel">
        <div class="MessageBody" id="aasdasdsa">

        </div>
    </div>
</div>

<template id="MyDataRowTemplate">
    <div>
        <span>{xx}</span>
        <br />{xx}
    </div>
</template>
<script>
    $(function () {
        UsingTemplateCreateNode("#MyDataRowTemplate", { xx: "撒扥僧扥所" }, "zzzzz", "#aasdasdsa");
    });
</script>
 */

// #endregion


///使用模板创建节点（优先模板上的占位符，占位符格式支持：${xxx.yyy.zzz}）
///templateSelector：模板选择器
///jObject:Json对象
///appendTo：新节点添加到
///newNodeId：新节点id
UsingTemplateCreateNode2 = function (templateSelector, jObject, appendTo, newNodeId) {
    var node = $(templateSelector).html();
    //[$][{][a-zA-Z$_][a-z.]{0,}[}]
    var items = node.match(/[$][{][a-zA-Z$_][a-z.]{0,}[}]/ig);
    if(items!=null){
        for (var i = 0;i<items.length;i++) {
            var item = items[i];
            var reg = new RegExp("\\"+item, "gi");
            var val =  getPathValue.call(jObject,item.substring(2,item.length-1));
            val = val ? val : "";
            if (/\/Date\([0-9]*\)/.test(val)) {
                val = parseDeteTime(val).Format("yyyy-MM-dd hh:mm:ss");
            }
            node = node.replace(reg, val);
        }
    }
    var $node = $(node);
    if (newNodeId) $node.attr("id", newNodeId);
    if (appendTo) $(appendTo).append($node);
    return $node;
}
/*Demo
 <div class="OAUI_DialogMask Active">
 <div class="PromptPanel">
 <div class="MessageBody" id="aasdasdsa">

 </div>
 </div>
 </div>

 <template id="MyDataRowTemplate">
 <div>
 <span>${xx}</span>
 <br />${xx}
 </div>
 </template>
 <script>
 $(function () {
 UsingTemplateCreateNode("#MyDataRowTemplate", { xx: "撒扥僧扥所" }, "zzzzz", "#aasdasdsa");
 });
 </script>
 */


//图片上传预览    IE是用了滤镜。
//file是图片上传控件，viewid是用于显示预览图片的div或span容器
function PreviewImage(file, viewId, maxWidth, maxHeight) {
    var MAXWIDTH = maxWidth ? maxWidth : 128;
    var MAXHEIGHT = maxHeight ? maxHeight : 128;
    var div = document.getElementById(viewId);
    if (file.files && file.files[0]) {
        var imgId = viewId + '_img';
        div.innerHTML = '<img id="' + imgId + '">';
        var img = document.getElementById(imgId);
        img.onload = function () {
            var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
            img.width = rect.width;
            img.height = rect.height;
            //img.style.marginLeft = rect.left+'px';
            img.style.marginTop = rect.top + 'px';
        }
        var reader = new FileReader();
        reader.onload = function (evt) { img.src = evt.target.result; }
        reader.readAsDataURL(file.files[0]);
    }
    else //兼容IE
    {
        var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
        file.select();
        var src = document.selection.createRange().text;
        div.innerHTML = '<img id=imghead>';
        var img = document.getElementById('imghead');
        img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
        var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
        status = ('rect:' + rect.top + ',' + rect.left + ',' + rect.width + ',' + rect.height);
        div.innerHTML = "<div id=divhead style='width:" + rect.width + "px;height:" + rect.height + "px;margin-top:" + rect.top + "px;" + sFilter + src + "\"'></div>";
    }
}
//图片缩放计算
function clacImgZoomParam(maxWidth, maxHeight, width, height) {
    var param = { top: 0, left: 0, width: width, height: height };
    if (width > maxWidth || height > maxHeight) {
        rateWidth = width / maxWidth;
        rateHeight = height / maxHeight;

        if (rateWidth > rateHeight) {
            param.width = maxWidth;
            param.height = Math.round(height / rateWidth);
        } else {
            param.width = Math.round(width / rateHeight);
            param.height = maxHeight;
        }
    }

    param.left = Math.round((maxWidth - param.width) / 2);
    param.top = Math.round((maxHeight - param.height) / 2);
    return param;
}

//图片上传预览    (不兼容旧浏览器)
//file是图片上传控件，viewId是用于显示图片的容器（图片作为背景显示）
function PreviewImage2(file, viewId) {
    var div = document.getElementById(viewId);
    var reader = new FileReader();
    reader.onload = function (evt) { div.style.backgroundImage = 'url(' + evt.target.result + ')'; }
    reader.readAsDataURL(file.files[0]);
}


//获取浏览器类型
function GetBrowserType() {
    if (navigator.userAgent.indexOf("MSIE") > 0) return "MSIE";//IE10及以下
    if (navigator.userAgent.indexOf("Trident") > 0) return "IE"; //IE11
    if (navigator.userAgent.indexOf("Edge") > 0) return "Edge"; //Edge
    if (navigator.userAgent.indexOf("Chrome") > 0) return "Chrome";
    if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) return "Firefox";
    if (isSafari = navigator.userAgent.indexOf("Safari") > 0) return "Safari";
    if (isCamino = navigator.userAgent.indexOf("Camino") > 0) return "Camino";
    if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) return "Gecko";
};